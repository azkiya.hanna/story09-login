from django.test import TestCase,Client
from django.urls import reverse,resolve
from .views import register
from .views import func

class LogIn(TestCase):
    
    # url profile
    def test_apakah_ada_url(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code,200)
    def test_apakah_ada_url_slash_register(self):
        c = Client()
        response = c.get('/register')
        self.assertEqual(response.status_code,200)
    def test_apakah_ada_url_untuk_login(self):
        c = Client()
        response = c.get('/accounts/login/')
        self.assertEqual(response.status_code,200)
    def test_apakah_ada_url_untuk_logout(self):
        c = Client()
        response = c.get('/logout/')
        self.assertEqual(response.status_code,200)
    def test_apakah_html_untuk_profile_digunakan(self):
        c = Client()
        response = c.get('')
        self.assertTemplateUsed(response, 'profile.html')
    def test_apakah_html_untuk_register_digunakan(self):
        c = Client()
        response = c.get('/register')
        self.assertTemplateUsed(response, 'registration/register.html')
    def test_apakah_fungsi_func_digunakan(self):
        response = resolve(reverse('profile'))
        self.assertEqual(response.func,func)
    def test_apakah_fungsi_register(self):
        response = resolve(reverse('register'))
        self.assertEqual(response.func,register)


       

# Create your tests here.
